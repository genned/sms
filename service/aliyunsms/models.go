package aliyunsms

type SendSmsArgs struct {
	Template      string            `json:"template"`
	TemplateParam map[string]string `json:"template_param"`
}

type Config struct {
	Template        map[string]*ConfigSms `json:"template"`
	AccessKeyID     string                `json:"access_key_id"`
	AccessKeySecret string                `json:"access_key_secret"`
}

type ConfigSms struct {
	SignName     string `json:"sign_name"`
	TemplateCode string `json:"template_code"`
}
