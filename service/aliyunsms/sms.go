package aliyunsms

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/denverdino/aliyungo/sms"
)

// SmsService # path:"/aloyunsms" #
type SmsService struct {
	cli  *sms.DYSmsClient
	conf *Config
}

func NewSmsService(conf *Config) *SmsService {
	return &SmsService{
		conf: conf,
		cli:  sms.NewDYSmsClient(conf.AccessKeyID, conf.AccessKeySecret),
	}
}

// Send a Sms #route:"POST /{phone}/{template}"#
func (s *SmsService) Send(phone, template string, body map[string]string) (err error) {
	temp, ok := s.conf.Template[template]
	if !ok {
		return errors.New("该短信模板不存在")
	}
	param, _ := json.Marshal(body)
	args := &sms.SendSmsArgs{
		PhoneNumbers:  phone,
		SignName:      temp.SignName,
		TemplateCode:  temp.TemplateCode,
		TemplateParam: string(param),
	}

	resp, err := s.cli.SendSms(args)
	if err != nil {
		return err
	}

	if resp.Code != "OK" {
		return fmt.Errorf("%s %s", resp.Code, resp.Message)
	}
	return nil
}
